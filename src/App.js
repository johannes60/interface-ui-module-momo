import './App.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import './index.css';
import axios from 'axios';





class NameForm extends React.Component {
  
  state = {
    amount: '',
    token:'',
    numero:'',
    message:'',
    isWaiting: false,
    returnUrl :'',
    back_link : '',
    techno_site : '',
  }

  componentDidMount(){
    
    var ReverseMd5 = require('reverse-md5');
    var rev = ReverseMd5({
      lettersUpper: false,
      lettersLower: true,
      numbers: true,
      special: false,
      whitespace: true,
      maxLen: 12
  });

    var str = window.location.href;
    var url = new URL(str);
    this.setState({isWaiting: false});
       
    var price = '';
    var token ='';

    if(url.search.includes('token='))
    {
      var data=url.search.split('&');
      if(data.length>0)
      {
        var tab = data[0].split('=');
        price = tab[1];
        price = rev(price);
        price = price['str'];

        var path = url.search.split('&')
        tab = path[2];
        path = tab.split('=')
        path = path[1];
        console.log(path);
        
       // tab = data[1].split('=');
        //token = tab[1];

        /*
        value= {this.state.amount}
        var path = url.search.split('&')
        tab = path[2];
        path = tab.split('=')
        path = path[1];
        console.log(path);
        this.setState({back_link: path});
        console.log(this.state.back_link);   */ 

        if(price == ' '){
          var amount = document.getElementById("amount");
          amount.value = " ";
        }else{
          var amount = document.getElementById("amount");
          amount.value = price;
          amount.setAttribute("readonly","");
        }    
          this.setState({amount: price});
          /*this.setState({token: token}); */
      }

    }

    let returnLink = document.getElementById("returnLink");
    returnLink.style.display="none";
}

  handleChange = event => {
    this.setState({ numero: event.target.value });
  }

  handleSubmit = event => {

    event.preventDefault();
    var msg = document.getElementById("message_request");

    /*if (!this.state.numero){
      msg.textContent = "Numéro invalide";
      msg.classList.add('alert-warning');
      return
    }*/
      
    let pay = document.getElementById("pay");    
    let loading = document.getElementById("loading");
    pay.style.display="none";
    msg.style.display="block";
    msg.textContent = "Transaction en cours";
    loading.style.display="block";
    msg.classList.add('alert-warning');
    this.setState({isWaiting: true});

    var str = window.location.href;
    var url = new URL(str);
    var techno = url.search.split('&')
    var tab;
    tab = techno[3];
    techno = tab.split('=')
    techno = techno[1];  

    var path = url.search.split('&')
    var tab_path;
    tab_path = path[2];
    path = tab_path.split('=')
    path = path[1];
    console.log(path);
    

    var data=url.search.split('&');
    var tab = data[0].split('=');
    tab = data[1].split('=');
    var token = tab[1];
    
   
    axios({

      method: 'post',
      url: 'https://api.dev.writer.prestashop.rintio.com/payment_request',
      data: {
          "amount": this.state.amount,
          "currency" : "XOF",
          "partyId": "229"+this.state.numero,
      }
    }).then((res) => {
      this.setState({isWaiting: false});
      var status = res.status;

      if( techno === 'Prestashop'){
        
        this.setState({returnUrl: path +'/index.php?fc=module&module=mobilepay&controller=validationAPI&status='+status+'&token='+token});
        console.log(this.state.returnUrl);
        }
     
     
      if(status === 200 ){
        msg.classList.remove('alert-warning');
        // msg.classList.add('alert-danger');
        loading.style.display="none";
         msg.textContent = "";   
        let popup_sucess = document.getElementById('popup_sucess');
        popup_sucess.style.right="3%";     
        msg.classList.remove('alert-danger');

        setTimeout(function(){
          let popup_sucess = document.getElementById('popup_sucess');
          popup_sucess.style.right="-27%";
     }, 2600);

        let returnLink = document.getElementById("returnLink");
        returnLink.style.display="block";
        returnLink.style.padding="5px";
        returnLink.style.marginTop="40px";
        returnLink.style.borderRadius="5px";
        returnLink.style.color="#2e9662";
        pay.style.display="none";

      }
  
    }, error => {
        console.log(error);
        let popup_failed = document.getElementById('popup_failed');
        popup_failed.style.right="3%";  
        loading.style.display="none";
        msg.textContent = "";     
        msg.classList.remove('alert-danger');       
        msg.classList.remove('alert-warning');
        msg.style.display="none";

      setTimeout(function(){
        let popup_failed = document.getElementById('popup_failed');
        popup_failed.style.right="-27%";
       }, 2600);

       if( techno === 'Prestashop'){
        var status = 500;      
        this.setState({returnUrl: path +'/index.php?fc=module&module=mobilepay&controller=validationAPI&status='+status+'&token='+token});
        console.log(this.state.returnUrl);
        }

       let returnLink = document.getElementById("returnLink");
       returnLink.style.display="block";
       returnLink.style.padding="5px";
       returnLink.style.marginTop="40px";
       returnLink.style.borderRadius="5px";
       returnLink.style.color="#FA8072";
       pay.style.display="none";

    })
   
  }
  
  render() {
    return (
      
      <div class="container-fluid">
      
      <div class="row">
         <div class="col-xs-12 col-md-3 col-lg-4"> </div>
         <div class="col-xs-12 col-md-6 col-lg-4">
           <div id="form">
             <div id="wow">
             <form  onSubmit={this.handleSubmit} class="fixed-center">
               <div class="card">
              

                     <div class="card-body">
                       <div id="form-main">
                           <div class="rintio">
                              <h3 class="title_card" >Paiement sécurisé par Mobile Money</h3>
                              <div id="padlock"></div>
                           </div> <hr/>  
                           
                               <div class="myImage"> </div>
                            
                         <div id="form-item">
                           <input type="tel"  placeholder="Numéro de téléphone (Ex: 66666530)" name="numero" onChange={this.handleChange} required  />
                         </div>
                         <div id="form-item">
                           <input type="tel" id="amount" />
                         </div>
                         <div class="loader" id="loading" style={{display: "none" }}>
                            <svg class="circular" viewBox="25 25 50 50">
                              <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                          </div>
                         <div id="form-footer">
                           <button type="submit" id="pay" class="pay" disabled={this.state.isWaiting === true ? true : false  }>Payer</button>
                           <button type="button" id="returnLink">
                             <a href={this.state.returnUrl}>Retourner à la boutique</a>
                             </button> <br></br>
                            <div class="alert" id="message_request" role="alert" style={{display: "none"}}></div>
                         </div>
                         </div>
                     </div>
               </div>
         </form>
         </div>
     </div>   
 </div>
   <div class="col-xs-4 col-md-3 col-lg-4"> </div>
 </div>
 <div id="popup_sucess"> <p id="message">Votre paiement a été validé</p></div>
 <div id="popup_failed"> <p id="message">Votre paiement a echoue</p></div>

 </div>

    
    );
  }
}

ReactDOM.render(
  <NameForm />,
  document.getElementById('root')
);

export default App;
